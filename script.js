   var categories = [
     {
       "id": 1,
       "name": "Game1",
       "image": "https://via.placeholder.com/200x100?text=Game1",
       "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan facilisis accumsan. Nulla lobortis, erat luctus ullamcorper gravida, tortor tellus egestas velit, eu iaculis ligula sapien vitae turpis",
       "filters": [
         {
           "name": "1000-2000",
           "products": [118145, 10000, 20000]
         },
         {
           "name": "2000-3000",
           "products": [118153]
         } 
       ]
     },
     {
       "id": 2,
       "name": "Game2",
       "image": "https://via.placeholder.com/200x100?text=Game2",
       "text": "Phasellus at dictum augue. Suspendisse eu dui in orci rutrum facilisis eu a libero. Mauris aliquam lectus ligula, nec dignissim justo fringilla at. ",
       "filters": [
         {
           "name": "4000-5000",
           "products": [118145]
         },
         {
           "name": "5000-6000",
           "products": [118153]
         }
       ]
     }
   ]
    
    
    var laptops = [
        {
            "name": "Lenovo IdeaPad S145-15IWL - 10000",
            "id": 10000,
            "part_num": 1,
            "colors": [
                {
                    color: 'gray',
                    text: 'сив',
                    id: 10000,
                    img: "https://www.pic.bg/image/show?f=1559199061.jpg&w=580&h=430"
                },
                {
                    color: 'black',
                    text: 'черен',
                    id: 135711,
                    img: "https://www.pic.bg/image/show?f=1565870726.jpg&w=580&h=430"
                }                  
            ],
            "url": "https://www.pic.bg/laptop-lenovo-ideapad-s145-15iwl-81mv003wbm-118145",
            "img": "https://www.pic.bg/image/show?f=1559199061.jpg&w=580&h=430",
            "cpu": "Intel® Celeron® Processor 4205U 1.80 GHz (2MB Cache)",
            "ram": "4GB 2133MHz Тип: DDR4",
            "display": "15.6\" (39.62 cm) HD Anti-Glare 1366x768 матов",
            "vga": "Intel® UHD Graphics 610",
            "hdd": "1000GB 5.4k rpm",
            "ssd": "Без SSD",
            "add_windows": true,
            "windows" : "Windows 10 Pro / 120 лв.",
            "variations": [
                {
                    ssd: 0,
                    price: 479,
                    label: "Базов",
                    label_ram: "4GB 2133MHz Тип: DDR4",
                    label_ssd: "Без SSD",
                    link: "/118145",
                    colors: [
                        { color: "gray",
                          id: 118145
                        },
                        { color: "black",
                          id: 135711
                        }
                    ]
                },
                {
                    ssd: 10,
                    price: 539,
                    label: "8GB RAM",
                    label_ram: "8GB 2133MHz Тип: DDR4",
                    label_ssd: "Без SSD",
                    link: "/124554",
                    colors: [
                        { color: "gray",
                          id: 124554
                        },
                        { color: "black",
                          id: 135886
                        }
                    ]
                },
                {
                    ssd: 120,
                    price: 559,
                    label: "8GB RAM и 120GB SSD",
                    label_ram: "8GB 2133MHz Тип: DDR4",
                    label_ssd: "120GB M.2 SSD",
                    link: "/124552",
                    colors: [
                        { color: "gray",
                          id: 124552
                        },
                        { color: "black",
                          id: 135714
                        }
                    ]
                },
                {
                    ssd: 240,
                    price: 589,
                    label: "8GB RAM и 240GB SSD",
                    label_ram: "8GB 2133MHz Тип: DDR4",
                    label_ssd: "240GB M.2 SSD",
                    link: "/124555",
                    colors: [
                        { color: "gray",
                          id: 124555
                        },
                        { color: "black",
                          id: 135887
                        }
                    ]
                }    
            ]
        },
        {
            "name": "Lenovo IdeaPad S145-15IWL",
            "id": 118145,
            "part_num": 1,
            "colors": [
                {
                    color: 'gray',
                    text: 'сив',
                    id: 118145,
                    img: "https://www.pic.bg/image/show?f=1559199061.jpg&w=580&h=430"
                },
                {
                    color: 'black',
                    text: 'черен',
                    id: 135711,
                    img: "https://www.pic.bg/image/show?f=1565870726.jpg&w=580&h=430"
                }                  
            ],
            "url": "https://www.pic.bg/laptop-lenovo-ideapad-s145-15iwl-81mv003wbm-118145",
            "img": "https://www.pic.bg/image/show?f=1559199061.jpg&w=580&h=430",
            "cpu": "Intel® Celeron® Processor 4205U 1.80 GHz (2MB Cache)",
            "ram": "4GB 2133MHz Тип: DDR4",
            "display": "15.6\" (39.62 cm) HD Anti-Glare 1366x768 матов",
            "vga": "Intel® UHD Graphics 610",
            "hdd": "1000GB 5.4k rpm",
            "ssd": "Без SSD",
            "add_windows": true,
            "windows" : "Windows 10 Pro / 120 лв.",
            "variations": [
                {
                    ssd: 0,
                    price: 479,
                    label: "Базов",
                    label_ram: "4GB 2133MHz Тип: DDR4",
                    label_ssd: "Без SSD",
                    link: "/118145",
                    colors: [
                        { color: "gray",
                          id: 118145
                        },
                        { color: "black",
                          id: 135711
                        }
                    ]
                },
                {
                    ssd: 10,
                    price: 539,
                    label: "8GB RAM",
                    label_ram: "8GB 2133MHz Тип: DDR4",
                    label_ssd: "Без SSD",
                    link: "/124554",
                    colors: [
                        { color: "gray",
                          id: 124554
                        },
                        { color: "black",
                          id: 135886
                        }
                    ]
                },
                {
                    ssd: 120,
                    price: 559,
                    label: "8GB RAM и 120GB SSD",
                    label_ram: "8GB 2133MHz Тип: DDR4",
                    label_ssd: "120GB M.2 SSD",
                    link: "/124552",
                    colors: [
                        { color: "gray",
                          id: 124552
                        },
                        { color: "black",
                          id: 135714
                        }
                    ]
                },
                {
                    ssd: 240,
                    price: 589,
                    label: "8GB RAM и 240GB SSD",
                    label_ram: "8GB 2133MHz Тип: DDR4",
                    label_ssd: "240GB M.2 SSD",
                    link: "/124555",
                    colors: [
                        { color: "gray",
                          id: 124555
                        },
                        { color: "black",
                          id: 135887
                        }
                    ]
                }    
            ]
        },
        {
            "name": "Lenovo IdeaPad S145-15IWL",
            "id": 118153,
            "part_num": 1,
            "colors": [
                {
                    color: 'black',
                    text: 'Черен',
                    id: 118153,
                    img: "https://www.pic.bg/image/show?f=1558103249.jpg&w=580&h=430"
                }  
            ],
            "url": "https://www.pic.bg/laptop-lenovo-ideapad-s145-15iwl-81mv0026bm-118153",
            "img": "https://www.pic.bg/image/show?f=1558103249.jpg&w=580&h=430",
            "cpu": "Intel® Core™ i5-8265U 1.60GHz up to 3.90GHz, 6MB cache (4-ядрен)",
            "ram": "8GB 2400MHz Тип: DDR4",
            "display": "15.6\" (39.62 cm) FullHD Anti-Glare 1920x1080 матов",
            "vga": "NVIDIA® GeForce® MX110 (2GB GDDR5)",
            "hdd": "1000GB 5.4k rpm",
            "ssd": "Без SSD",
            "variations": [
                {
                    ssd: 2,
                    price: 1089,
                    label: "Базов",
                    label_ram: "8GB 2400MHz Тип: DDR4",
                    label_ssd: "Без SSD",
                    link: "/118153",
                    colors: [
                        { color: "black",
                          id: 118153
                        }
                    ]
                },
                {
                    ssd: 120,
                    price: 1139,
                    label: "120GB SSD",
                    label_ram: "8GB 2400MHz Тип: DDR4",
                    label_ssd: "120GB SSD",
                    link: "/123097",
                    colors: [
                        { color: "black",
                          id: 123097
                        }
                    ]
                },
                {
                    ssd: 240,
                    price: 1169,
                    label: "240 M.2 SSD",
                    label_ram: "8GB 2400MHz Тип: DDR4",
                    label_ssd: "256GB M.2 SSD",
                    link: "/123098",
                    colors: [
                        { color: "black",
                          id: 123098
                        }
                    ]
                },{
                    ssd: 241,
                    price: 1289,
                    label: "16GB RAM и 240GB SSD",
                    label_ram: "16GB 2400MHz Тип: DDR4",
                    label_ssd: "256GB NVMe PCIe SSD",
                    link: "/133311",
                    colors: [
                        { color: "black",
                          id: 133311
                        }
                    ]
                }
            ]
        } 
    ];
    var products = [];
    var color = {};
    var value = {};
    var type = 'ssd';
    
    for (var i = 0; i < laptops.length; i++) {
        var laptop = laptops[i];
        //console.log(laptop);
        var html = '<div class="row product-cust-containers product-container product-container-'+laptop.id+'">';
        html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
        html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
        html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";
        
        if (laptop.colors.length > 1) {
            html += '<div class="row thumbnail-container text-center">';
            html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
            for (var lc = 0; lc < laptop.colors.length; lc++) {
                var laptop_color = laptop.colors[lc];
                color[laptop_color.id] = laptop_color.color;
                value[laptop_color.id] = 0;
                html += "<div class='col-xs-3 col-centered'>";
                html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
                html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
                html += "</div>";
            }
            html += '</div>';
        }
        html += '</div>';
        html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
        html += "<ul class='text-left list list-unstyled landing-description'>";
        html += "<li class='cpu-info'><span>Процесор</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.cpu + "' data-original-title='test'></span><br /> <p class='info-mobile-align'>" + laptop.cpu + "</p></li>"; 
        html += "<li><span>Екран</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.display + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.display + "</p> </li>";
        html += "<li><span>Памет</span><br /> <p class='info-mobile-align description-ram-" + laptop.id + "'>" + laptop.ram + "</p></li>";
        html += "<li><span>Видеокарта</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
        html += "<li><span>SSD</span><div> <p class='info-mobile-align description-ssd-" + laptop.id + "'>" + laptop.ssd + "</p></div></li>";
    
        html += "<li><span>Хардк диск</span><br /> <p class='info-mobile-align'>" + laptop.hdd + "</p></li>";
        html += "</ul>";
        html += "<div class='choose-upgrade'>";
        html += '<i aria-hidden="true" class="fa fa-hdd-o hdd-acer">&nbsp;</i> <span class="choose-model-aspire5">Избери UPGRADE</span>';
        html += '</div>';
        html += "<div class='button-section'>";
        html += '<br /><ul class="nav nav-tabs text-left text-sm-center" role="tablist">';
    
        var variations = laptop.variations;
    
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            
            var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
            var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
            var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;
            
            html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-price="'+ssd_variation.price+'" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';
            
            //if(ssd_variation.colors.length>1) {
                for (var c = 0; c < ssd_variation.colors.length; c++) {
                    var ssd_variation_color = ssd_variation.colors[c];
                    products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
                }
            //}
            if(ssd_variation.colors.length==1) {
                products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
            }

        }
        html += '</ul>';
        if(laptop.add_windows == true) {
          html += '<div class="checkbox"><label><input type="checkbox" name="windows" class="windows" id="windows-'+laptop.id+'" data-id="' + laptop.id + '"/>'+laptop.windows+'</label>';
        }
        html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
            html += '<div class="row custom-buttonWrapper-checkout">';
            html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';
    
            html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        html += "</div>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
        html += "<br /><br />";
        $("#products").append(html);
    }
    
    $(function () {
      
      
        // Categories start
        for(var i=0; i<categories.length; i++) {
          var category = categories[i];
          var cat = '';
          cat += '<div class="col-xs-4 col-sm-3 category-container category-container-'+category.id+'">';
          cat += '<img src="'+category.image+'" class="img-responsive category-img" data-category="'+category.id+'" />';
          cat += '</div>';
          $('#categories').append(cat);
        }
        
        $(document).on('click', '.category-img', function(){
          
          var categoryID  = $(this).data('category');
          $('.category-container').hide();
          $('.all-categories-btn').show();
          var category = getObjectByID('category', categoryID)[0];
          if(typeof(category)!="undefined") {
            
            $('.product-container').hide();
            var filters  = category.filters;           
            if(typeof(filters)!='undefined') {
              var prdArr = [];
               for(var fi=0; fi<filters.length; fi++) {
                 var filter = filters[fi];
                 if(typeof(filter.products)!='undefined') {
                  prdArr = prdArr.concat(filter.products);
                }
             }
           }
           showProducts(prdArr);

            // Banner
            var categoryInfo = '';
            categoryInfo += '<div class="col-xs-6 col-sm-3">';
            categoryInfo += '<img src="'+category.image+'" class="img-responsive category-img" data-category="'+category.id+'" />';
            categoryInfo += '</div>';
            categoryInfo += '<div class="col-xs-6 col-sm-9 text-left">';
            categoryInfo += category.text;
            categoryInfo += '</div>';
            $('#category-info').html(categoryInfo);

            // Filters
            var filters = category.filters;
            var filterBtns = '';
            for(var i=0; i<filters.length; i++) {
              var filter = filters[i];
              console.log(filter);
              filterBtns += '<div class="col-xs-3 col-sm-1">';
              filterBtns += '<button class="btn category-filter" data-products="'+filter.products+'" data-category="'+category.id+'">'+filter.name+'</button>';
              filterBtns += '</div>';
            }
            $('#category-filters').html(filterBtns);
            
          }
          
        });
        
        $(document).on('click', '.category-filter', function(){
          
          var categoryID  = $(this).data('category');
          console.log(categoryID);
          var products  = $(this).data('products').toString();
          console.log(products);
          var productsArr = products.split(',');
          showProducts(productsArr);
        });
        
        function showProducts(productsArr) {
          
          if(typeof(productsArr)!="undefined") {
            console.log(productsArr);
            $('.product-container').hide();
            for(var i=0; i<productsArr.length; i++) {
              var prd = productsArr[i];
              console.log('Show product: ' + prd);
              $('.product-container-'+prd).show();
            }
          }
          
        }
        
        $(document).on('click', '.all-categories-btn', function(){
          $(this).hide();
          $('.category-container').show();
          $('#category-info').html('');
          $('#category-filters').html('');
          $('.product-container').hide();
        });
        
        // Categories end
        
    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            setVariant($(e.target));
        });
    
        function setVariant(el) {
            var ssd = el.data('ssd');
            var ram = el.data('ram');
            var hdd = el.data('hdd');
            var id = el.data('id');
            var price = el.data('price');

            var t = el.data('type');
            var c = color[id];
            var v = el.data('value');
            value[id] = v;
            type[id] = t;
            if (typeof (c) != "undefined") {
                var k = id + '' + '' + c + '' + v;
            }
            else {
                var k = id + 'default' + v;
            }
            if (ssd != '') {
                $('.description-ssd-' + id).html(ssd);
            }
            if (ram != '') {
                $('.description-ram-' + id).html(ram);
            }
            if (hdd != '') {
                $('.description-hdd-' + id).html(hdd);
            }
            
            // Windows
            var windows = $('#windows-'+id);
            var windowsPrefix = '';
            if(windows.length) {
              if(windows.is(":checked")) {
                console.log('ADD WINDOWS');
                $('.tab-content-' + id + ' .price-tag-label').html(price + 120);
                windowsPrefix = '900';
              }
              else {
                console.log('REMOVE WINDOWS');
                $('.tab-content-' + id + ' .price-tag-label').html(price);
              }
            }
            $('.tab-content-' + id + ' .tab-pane.active a').attr('href', "/products/" + windowsPrefix + products[k]);
        }
        
        $('.windows').on('click', function(){
          var id = $(this).data('id');
          var price = parseInt($('.tab-content-' + id + ' .price-tag-label').html());
          var href = $('.tab-content-' + id + ' .tab-pane.active a').attr('href');
          var parts = href.split('/');
          var product = parts[parts.length-1];
          
          if($(this).is(":checked")) {
            console.log('ADD WINDOWS');
            console.log($('.tab-content-' + id + ' .price-tag-label').html());
            price = price + 120;
            product = '900'+product;
          }
          else {
            console.log('REMOVE WINDOWS');
            price = price - 120;
            if(product.indexOf('900')==0) {
              product = product.substr(3);
            }
          }
          $('.tab-content-' + id + ' .price-tag-label').html(price);
          $('.tab-content-' + id + ' .tab-pane.active a').attr('href', '/'+product);
        }); 
       
        $('a.variant').on('click', function (e) {
            setVariant($(this));
        });
    
        // product colors script
        $('.img-thumb-color').on('click', function (e) { 
            var colorId = $(this).data('colorid'); // Base color ID
            var id = $(this).data('id'); // Base product ID
            var c = $(this).data('color');
            color[id] = c;
            var k = id+''+''+c+''+value[id];
            var newProductId = products[k];
            if(typeof(newProductId)=='undefined') {
                newProductId = colorId;
            }
            $('.big-img-' + id).attr('src', $(this).attr('src'));
            
            // Windows
            var windows = $('#windows-'+id);
            var windowsPrefix = '';
            if(windows.length) {
              if(windows.is(":checked")) {
                console.log('ADD WINDOWS');
                windowsPrefix = '900';
              }
              else {
                console.log('REMOVE WINDOWS');
              }
            }
           
            
            $('.tab-content-' + id + ' .tab-pane.active a').attr('href', "/products/" + windowsPrefix + newProductId);
        });
    });
    
             
    
    function getObjectByID(type, id) {
      if(type=='laptop') {
        return laptops.filter(
            function(laptops){ return laptops.id == id }
        );
      }
      if(type=='category') {
        return categories.filter(
            function(categories){ return categories.id == id }
        );
      }
    }